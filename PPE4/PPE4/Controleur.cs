﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PPE4
{
    /// <summary>
    /// Classe Controleur qui gère le lien entre les vues et le MODELE
    /// </summary>
    public static class Controleur
    {
        #region propriétés
        private static Modele vmodele;
        #endregion

        #region accesseurs
        /// <summary>
        /// propriété Vmodele
        /// </summary>
        public static Modele Vmodele
        {
            get { return vmodele; }
            set { vmodele = value; }
        }
        #endregion

        #region methodes
        /// <summary>
        /// instanciation du modele
        /// </summary>
        public static void init()
        {
            Vmodele = new Modele();
        }
        /// <summary>
        /// Méthode de déconnexion à la BDD
        /// </summary>
        public static void Deconnexion()
        {
            if (Vmodele.Connopen)    // si connexion ouverte
            {
                Vmodele.sedeconnecter();
            }
        }
        #endregion
        public static void crud_PR(Char c, int indice)
        {
            FormCRUD formCRUD = new FormCRUD(); // création de la nouvelle forme
            if (c == 'u') // mode update ou donc on récupère les champs
            {
                formCRUD.TextBoxCommentaire.Text = vmodele.DT[0].Rows[indice][9].ToString();
                formCRUD.NumericUpDownNbEtoiles.Value = Convert.ToDecimal(vmodele.DT[0].Rows[indice][10]);
            }
            // on affiche la nouvelle form
            formCRUD.ShowDialog();

            // si l’utilisateur clique sur OK
            if (formCRUD.DialogResult == DialogResult.OK)
              {
                  if (c == 'u') // mode modification
                  {
                      // on met à jour le dataTable avec les nouvelles valeurs saisies dans formCRUD
                      vmodele.DT[0].Rows[indice]["Commentaire"] = formCRUD.TextBoxCommentaire.Text;
                      vmodele.DT[0].Rows[indice]["Nombre d'étoiles"] = Convert.ToDecimal(formCRUD.NumericUpDownNbEtoiles.Value);
                      vmodele.DA[0].Update(vmodele.DT[0]);
                  }
                  MessageBox.Show("OK : données enregistrées");
                  formCRUD.Dispose();
              }
              else
              {
                  MessageBox.Show("Annulation : aucune donnée enregistrée");
                  formCRUD.Dispose();
              }
        }

    }
}
