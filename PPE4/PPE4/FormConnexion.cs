﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;
using System.Runtime.InteropServices;

namespace PPE4
{
    public partial class FormConnexion : Form
    {
        #region Déplacer la fenetre
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();
        #endregion

        public FormConnexion()
        {
            InitializeComponent();
        }

        private void FormConnexion_Load(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            textBox2.UseSystemPasswordChar = true;

        }

        private void buttonConnexion_Click(object sender, EventArgs e)
        {
            Controleur.init();
            Controleur.Vmodele.seconnecter();

            if (Controleur.Vmodele.Connopen == false)
            {
                MessageBox.Show("Connexion HS !!!");
            }
            else
            {
                MySqlDataAdapter sda = new MySqlDataAdapter("Select Count(*) From connexion where identifiant_connexion='" + textBox1.Text + "' AND mdp_connexion ='" + textBox2.Text + "';", Controleur.Vmodele.MyConnection);
                DataTable dt = new DataTable();
                sda.Fill(dt);

                if (dt.Rows[0][0].ToString() == "1")
                {
                    MessageBox.Show("Bienvenue inspecteur !");
                    FormPrincipale FP = new FormPrincipale(textBox1.Text);
                    FP.Show();
                    this.Hide();
                    

                    Controleur.Vmodele.import(textBox1.Text);
                    if (Controleur.Vmodele.Chargement)
                    {
                        MessageBox.Show("Importation OK !!!");
                    }
                    else
                    {
                        MessageBox.Show("Erreur import !!!");
                    }
                }
                else
                {
                    MessageBox.Show("Merci de réessayer !");
                }
            }
        }

        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            #region bouger la fenetre
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
            #endregion
        }
    }
}
