﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Runtime.InteropServices;


namespace PPE4
{
    public partial class FormHistorique : Form
    {
        #region Déplacer la fenetre
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();
        #endregion
        private BindingSource bindingSource2 = new BindingSource();

        public FormHistorique()
        {
            InitializeComponent();
        }

        private void FormHistorique_Load(object sender, EventArgs e)
        {
            chargeDonnees();
        }
        /// <summary>
        /// Chargement des données dans la DataGridView1
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        public void chargeDonnees()
        {
            #region chargement comboBox
            comboBox1.Items.Clear();
            List<KeyValuePair<int, string>> uList = new List<KeyValuePair<int, string>>();
            uList.Add(new KeyValuePair<int, string>(0, "N° de departement"));
            comboBox1.Items.Add("N° de departement");
            for (int i = 0; i < Controleur.Vmodele.Dv_departement.Count; i++)
            {
                uList.Add(new KeyValuePair<int, string>((int)Controleur.Vmodele.Dv_departement.ToTable().Rows[i][0], Controleur.Vmodele.Dv_departement.ToTable().Rows[i][1].ToString()));
            }
            comboBox1.DataSource = uList;
            comboBox1.ValueMember = "Key";
            comboBox1.DisplayMember = "Value";
            comboBox1.Text = comboBox1.Items[0].ToString();

            comboBox1.DropDownStyle = ComboBoxStyle.DropDownList;
            #endregion
            #region chargement DataGridView
            if (Controleur.Vmodele.Chargement)
            {
                // un DT par table
                bindingSource2 = new BindingSource();

                bindingSource2.DataSource = Controleur.Vmodele.Dv_historique;
                dataGridView1.DataSource = bindingSource2;
            }
            // mise à jour du dataGridView via le bindingSource rempli par le DataTable
            dataGridView1.Refresh();
            #endregion
        }

        private void changefiltre()
        {
            string num = comboBox1.SelectedValue.ToString();

            int n = Convert.ToInt32(num);
            if (n == 0) // cas de "nums departement"
                Controleur.Vmodele.Dv_historique.RowFilter = "";
            else
            {
                string Filter = "num_semaine = '" + n + "'";
                Controleur.Vmodele.Dv_historique.RowFilter = Filter;
            }
            dataGridView1.Refresh();
        }

        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            #region bouger la fenetre
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
            #endregion
        }

        private void label4_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
