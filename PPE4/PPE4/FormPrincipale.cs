﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Runtime.InteropServices;

namespace PPE4
{
    /// <summary>
    /// Forme Principale avec affichage liste des visites + selection tri par semaine
    /// </summary>
    public partial class FormPrincipale : Form
    {
        #region Déplacer la fenetre
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();
        #endregion
        private BindingSource bindingSource1 = new BindingSource();

        public FormPrincipale(string user)
        {
            InitializeComponent();
            label4.Text = user;
        }


        private void FormPrincipale_Load(object sender, EventArgs e)
        {
            var User2 = label4.Text;
            Controleur.Vmodele.import(User2);
            chargeDonnees();
            

        }

        /// <summary>
        /// Chargement des données dans la DataGridView1
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        public void chargeDonnees()
        {
            #region chargement comboBox
            comboBoxNumSemaine.Items.Clear();
            List<KeyValuePair<int, string>> uList = new List<KeyValuePair<int, string>>();
            uList.Add(new KeyValuePair<int, string>(0, "N° de semaine"));
            comboBoxNumSemaine.Items.Add("N° de semaine");
            for (int i = 0; i < Controleur.Vmodele.Dv_semaine.Count; i++)
            {
                uList.Add(new KeyValuePair<int, string>((int)Controleur.Vmodele.Dv_semaine.ToTable().Rows[i][0], Controleur.Vmodele.Dv_semaine.ToTable().Rows[i][1].ToString()));
            }
            comboBoxNumSemaine.DataSource = uList;
            comboBoxNumSemaine.ValueMember = "Key";
            comboBoxNumSemaine.DisplayMember = "Value";
            comboBoxNumSemaine.Text = comboBoxNumSemaine.Items[0].ToString();

            comboBoxNumSemaine.DropDownStyle = ComboBoxStyle.DropDownList;
            #endregion
            #region chargement DataGridView
            if (Controleur.Vmodele.Chargement)
            {
                // un DT par table
                bindingSource1 = new BindingSource();

                bindingSource1.DataSource = Controleur.Vmodele.Dv_listevisiteparinspecteur;
                dataGridView1.DataSource = bindingSource1;
                dataGridView1.Columns[0].HeaderText = "id_visites";
                dataGridView1.Columns[0].Visible = false;

                dataGridView1.Columns[1].HeaderText = "Semaine n°";
                dataGridView1.Columns[2].HeaderText = "Date prévue";
                dataGridView1.Columns[3].HeaderText = "Nom";
                dataGridView1.Columns[4].HeaderText = "Adresse";
                dataGridView1.Columns[5].HeaderText = "Code postal";
                dataGridView1.Columns[6].HeaderText = "Ville";
                dataGridView1.Columns[7].HeaderText = "N° de tél";
                dataGridView1.Columns[8].HeaderText = "E-mail";
                dataGridView1.Columns[9].Visible = false; //identifiant_connexion
                dataGridView1.Columns[10].HeaderText = "Commentaire";
                dataGridView1.Columns[11].HeaderText = "Nombre d'étoiles";
            }
            // mise à jour du dataGridView via le bindingSource rempli par le DataTable
            dataGridView1.Refresh();
            #endregion
        }

        private void changefiltre()
        {
            string num = comboBoxNumSemaine.SelectedValue.ToString();

            int n = Convert.ToInt32(num);
            if (n == 0) // cas de "nums semaine"
                Controleur.Vmodele.Dv_listevisiteparinspecteur.RowFilter = "";
            else
            {
                string Filter = "num_semaine = '" + n + "'";
                Controleur.Vmodele.Dv_listevisiteparinspecteur.RowFilter = Filter;
            }
            dataGridView1.Refresh();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void comboBoxNumSemaine_SelectionChangeCommitted(object sender, EventArgs e)
        {
            changefiltre();
        }

        private void buttonAccueil_Click(object sender, EventArgs e)
        {
            FormPrincipale FP = new FormPrincipale(label4.Text);
            FP.Show();
        }

        private void buttonChoixVisite_Click(object sender, EventArgs e)
        {
            FormCRUD FC = new FormCRUD();
            FC.Show();
        }

        private void buttonHistorique_Click(object sender, EventArgs e)
        {
            FormHistorique H = new FormHistorique();
            H.Show();
        }

        private void pictureBox1_Click_1(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void buttonChoixVisite_Click_1(object sender, EventArgs e)
        {
            // vérifier qu’une ligne est bien sélectionnée dans le dataGridView
            if (dataGridView1.SelectedRows.Count == 1)
            {
                Controleur.crud_PR('u', Convert.ToInt32(dataGridView1.SelectedRows[0].Index));
                // mise à jour du dataGridView en affichage
                bindingSource1.MoveLast();
                bindingSource1.MoveFirst();
                dataGridView1.Refresh();
                FormCRUD FC = new FormCRUD();
                FC.Show();
            }
            else
            {
                MessageBox.Show("Sélectionner une ligne à modifier");
            }

        }

        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            #region suite déplacement
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
            #endregion
        }

        private void buttonHistorique_Click_1(object sender, EventArgs e)
        {
            FormHistorique FH = new FormHistorique();
            FH.Show();
        }
    }
}
