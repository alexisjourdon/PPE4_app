﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Data;
using System.Windows.Forms;
namespace PPE4
{
    /// <summary>
    /// Modele Classe : Application des inspecteurs
    /// </summary>
    public class Modele
    {

        #region propriétés

        private MySqlConnection myConnection;   // objet de connexion
        private bool connopen = false;          // test si la connexion est faite
        private bool errgrave = false;          // test si erreur lors de la connexion
        private bool chargement = false;        // test si le chargement d'une requête est fait

        // les DataAdapter et DataTable seront gérés dans des collections avec pour chaque un indice correspondant :
        // indice 0 : récupération des noms des tables
        // indice 1 : Table Constructeur
        // indice 2 : Table Support avec jointure pour récupérer tous les libellés
        // indice 3 : Table Support

        // collection de DataAdapter
        private List<MySqlDataAdapter> dA = new List<MySqlDataAdapter>();

        // collection de DataTable récupérant les données correspond au DA associé
        private List<DataTable> dT = new List<DataTable>();


        private MySqlDataAdapter mySqlDataAdapterImport = new MySqlDataAdapter();
        private DataSet dataSetImport = new DataSet();
        private DataView dv_visites = new DataView(), dv_listevisiteparinspecteur = new DataView(), dv_semaine = new DataView(), dv_historique = new DataView(), dv_departement = new DataView();

        private char vaction, vtable;


        #endregion

        #region accesseurs
        public MySqlConnection MyConnection { get => myConnection; set => myConnection = value; }
        public DataView Dv_visites { get => dv_visites; set => dv_visites = value; }
        public DataView Dv_listevisiteparinspecteur { get => dv_listevisiteparinspecteur; set => dv_listevisiteparinspecteur = value; }
        public char Vaction { get => vaction; set => vaction = value; }
        public char Vtable { get => vtable; set => vtable = value; }
        public MySqlConnection MyConnection1 { get => myConnection; set => myConnection = value; }

        public bool Connopen { get => connopen; set => connopen = value; }
        public bool Errgrave { get => errgrave; set => errgrave = value; }
        public bool Chargement { get => chargement; set => chargement = value; }
        public DataView Dv_semaine { get => dv_semaine; set => dv_semaine = value; }
        public List<MySqlDataAdapter> DA { get => dA; set => dA = value; }
        public List<DataTable> DT { get => dT; set => dT = value; }
        public DataView Dv_historique { get => dv_historique; set => dv_historique = value; }
        public DataView Dv_departement { get => dv_departement; set => dv_departement = value; }



        #endregion

        public Modele()
        {

            for (int i = 0; i < 60; i++)
            {
                dA.Add(new MySqlDataAdapter());
                dT.Add(new DataTable());
            }
        }

        /// <summary>
        /// méthode seconnecter permettant la connexion à la BD : bd_ppe3_THT
        /// </summary>
        public void seconnecter()
        {
            string myConnectionString = "Database=ppe4_jourdon;Data Source=192.168.203.1;User Id=alexis;password=jourdon";
            myConnection = new MySqlConnection(myConnectionString);
            try // tentative 
            {
                myConnection.Open();
                connopen = true;
            }
            catch (Exception err)// gestion des erreurs
            {
                MessageBox.Show("Erreur ouverture BD PPE4 : " + err, "PBS connection", MessageBoxButtons.OK, MessageBoxIcon.Error);
                connopen = false; errgrave = true;
            }
        }

        /// <summary>
        /// méthode sedeconnecter pour se déconnecter à la BD
        /// </summary>
        public void sedeconnecter()
        {
            if (!connopen)
                return;
            try
            {
                myConnection.Close();
                myConnection.Dispose();
                connopen = false;
            }
            catch (Exception err)
            {
                MessageBox.Show("Erreur fermeture BD PPE4 : " + err, "PBS deconnection", MessageBoxButtons.OK, MessageBoxIcon.Error);
                errgrave = true;
            }
        }

        /// <summary>
        /// méthode générique privée pour charger le résultat d'une requête dans un dataTable via un dataAdapter
        /// Méthode appelée par charger_donnees(string table)
        /// </summary>
        /// <param name="requete">requete à charger</param>
        /// <param name="DT">dataTable</param>
        /// <param name="DA">dataAdapter</param>
        private void charger(string requete, DataTable DT, MySqlDataAdapter DA)
        {
            DA.SelectCommand = new MySqlCommand(requete, myConnection);

            // pour spécifier les instructions de mise à jour (insert, delete, update)
            MySqlCommandBuilder CB1 = new MySqlCommandBuilder(DA);
            try
            {
                DT.Clear();
                DA.Fill(DT);
                chargement = true;
            }
            catch (Exception err)
            {
                MessageBox.Show("Erreur chargement dataTable : " + err, "PBS table", MessageBoxButtons.OK, MessageBoxIcon.Error);
                errgrave = true;
            }
        }

        /// <summary>
        /// charge dans un DT les données de la table passée en paramètre
        /// </summary>
        /// <param name="table">nom de la table à requêter</param>
        public void charger_donnees(string table)
        {
            chargement = false;
            if (!connopen) return;		// pour vérifier que la BD est bien ouverte

            if (table == "listevisites")
            {
                charger("select * from listevisiteparinspecteur;", dT[0], dA[0]);
            }

            if (table == "semaine")
            {
                charger("select num_semaine from semaine;", dT[2], dA[2]);
            }
            if (table == "historique")
            {
                charger("select * from historiquevisite;", dT[3], dA[3]);
            }
            if (table == "departement")
            {
                charger("select departement_code from departement;", dT[4], dA[4]);
            }


        }

        /// <summary>
        /// Importation des données concernant les coureurs et leur équipe en vue d'une utilisation en mode déconnecté
        /// L'importation se fait dans des dataView, autant de dataView que de requêtes faites
        /// </summary>
        public void import(string user2)
        {
            if (!connopen)
                return;
            mySqlDataAdapterImport.SelectCommand = new MySqlCommand("select * from listevisiteparinspecteur where identifiant_connexion = '" + user2 + "';select * from semaine;select * from historiquevisite;select * from departement;", myConnection);
            MySqlCommandBuilder CB1 = new MySqlCommandBuilder(mySqlDataAdapterImport);
            try
            {
                dataSetImport.Clear();
                mySqlDataAdapterImport.Fill(dataSetImport);
                MySqlCommand vcommand = myConnection.CreateCommand();


                dv_listevisiteparinspecteur = dataSetImport.Tables[0].DefaultView;
                dv_semaine = dataSetImport.Tables[1].DefaultView;
                dv_historique = dataSetImport.Tables[3].DefaultView;
                dv_departement = dataSetImport.Tables[3].DefaultView;



                chargement = true;
            }
            catch (Exception err)
            {
                MessageBox.Show("Erreur chargement dataset : " + err, "Probleme d'import des BDD", MessageBoxButtons.OK, MessageBoxIcon.Error);
                errgrave = true;
            }
        }


        /// <summary>
        /// Exportation des données des dataView pour remettre à jour les données dans la BD
        /// </summary>
       /* public void export()
        {
            if (!connopen)
                return;
            try
            {
                maj_equipes();
            }
            catch (Exception err)
            {
                MessageBox.Show("erreur dans l'export maj equipes" + err);
            }
        }*/

        /// <summary>
        /// Méthode pour mettre à jour les données du dataView dv_equipes (modifié en mode déconnecté) dans la table EQUIPE de la BDD
        /// </summary>
        /*public void maj_equipes()
        {
            vaction = 'u'; vtable = 'e';
            if (!connopen) return;

            // préparation de la requête avec les paramètres pour mettre à jour le champ "Au_Depart"
            mySqlDataAdapterImport.UpdateCommand = new MySqlCommand("update equipe set AU_DEPART=?Au_Depart where IdEqu = ?IdEqu", myConnection);// notre commandbuider ici update non fait si maj ou delete dans la base

            // déclaration des paramètres utiles au commandbuilder
            mySqlDataAdapterImport.UpdateCommand.Parameters.Add("?IdEqu", MySqlDbType.Int32, 10, "IDEQU");
            mySqlDataAdapterImport.UpdateCommand.Parameters.Add("?Au_Depart", MySqlDbType.Int16, 4, "AU_DEPART");


            //on continue même si erreur de MAJ
            mySqlDataAdapterImport.ContinueUpdateOnError = true;

            //table concernée 1 = equipe
            DataTable table = dataSetImport.Tables[1];
            //on ne s'occupe que des enregistrements modifiés en local dans la table du dataView concernée
            mySqlDataAdapterImport.Update(table.Select(null, null, DataViewRowState.ModifiedCurrent));
        }*/

    }
}

